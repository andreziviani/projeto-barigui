# Guia rápido de uso

## Virtualização (OpenNebula)
O controle das máquinas virtuais é feito através da ferramenta [OpenNebula] ([documentação OpenNebula]).
Ao acessar a ferramenta será necessário realizar o login.

![Tela de login da ferramenta OpenNebula][login OpenNebula]

Após o login será mostrado um dashboard com a visão geral da infraestrutura.

![Dashboard geral da infraestrutura][dashboard OpenNebula]

Clique no menu "VMs", mostrado na figura acima.

![Visão geral das VMs][vms OpenNebula]

Para criar uma VM clique no botão "+", mostrado na figura acima

![Criação de uma VM][criar vm OpenNebula]

Primeiro selecione o template "template-docker" (1), em seguida especifique um nome para a VM (2), ajuste a quantidade memória (3), CPU (4) e disco (5). Por fim clique no botão "Create" (6). Após alguns segundos a VM está com Status "Running". Após esse momento a VM já se encontra acessível através do IP mostrado ao lado do nome da VM, a VM também já aparece automaticamente no painel de monitoramento.

## Monitoramento (Prometheus + Grafana)
O monitoramento é feito utilizando a ferramenta [Prometheus] ([documentação Prometheus]) e os dados são visualizados nos dashboards do [Grafana] ([documentação Grafana]).

![Tela de login da ferramenta Grafana][login Grafana]

Após efetuar o login, o dashboard sobre as VMs será carregado.

![Dashboard de monitoramento][dashboard Grafana]

Para visualizar outra VM, selecione no menu Host (1), para alterar o range de tempo e o refresh clique em (2), para dar um refresh manual nos gráficos clique em (3) e para ver outros dashboards clique em (4).

[documentação OpenNebula]: <https://opennebula.org>
[OpenNebula]: <https://infra.ziviani.com.br>
[login OpenNebula]: <https://gitlab.com/andreziviani/projeto-barigui/raw/master/figs/01-login.png>
[dashboard OpenNebula]: <https://gitlab.com/andreziviani/projeto-barigui/raw/master/figs/02-vms.png>
[vms OpenNebula]: <https://gitlab.com/andreziviani/projeto-barigui/raw/master/figs/03-list_vms.png>
[criar vm OpenNebula]: <https://gitlab.com/andreziviani/projeto-barigui/raw/master/figs/04-create_vm.png>
[Prometheus]: <https://infra.ziviani.com.br/prometheus>
[documentação Prometheus]: <https://prometheus.io/>
[Grafana]: <https://infra.ziviani.com.br/grafana>
[documentação Grafana]: <https://grafana.com/>
[login Grafana]: <https://gitlab.com/andreziviani/projeto-barigui/raw/master/figs/05-grafana.png>
[dashboard Grafana]: <https://gitlab.com/andreziviani/projeto-barigui/raw/master/figs/06-grafana_visualizacao.png>
