# Projeto de Infraestrutura para o Grupo Barigui

Esse projeto contempla a descrição e implementação de uma infraestrutura remota de produção e outra local para testes e desenvolvimento. Ambas as infraestrutras, local e remota, devem ser o mais similar possível para que não haja comlicações no momento da publicação de uma nova versão do ambiente.

## Infraestrutura Remota

Existe diversas formas de estruturar uma infraestrutura, cada uma com seus prós e contras dependendo do tipo de serviço (aplicação) que será executada nela. Algumas opções são a *serverless*, container, VMs, entre outras.

### Serverless

Acredito que a arquitetura *serverless* esteja se tornando uma forte candidata em sistemas de alto desempenho e carga, um ótimo exemplo é o serviço [Have I Been Pwned] desenvolvido por [Troy Hunt] utilizando [Microsoft Azure Functions] e [Cloudflare Workers].

Esse modelo funciona muito bem pois o valor cobrado é apenas pelo número de requests atendidos e o Cloudflare consegue reduzir muito esse número devido ao seu cache e *workers* trabalhando próximos ao usuário trazendo também uma redução considerável na latência da conexão. Também possui a vantagem de trivialmente automatizado.

Algumas desvantagens desse modelo são que é necessário desenvolver a aplicação pensando nessa arquitetura ou adaptar uma aplicação já existente, porém nem sempre isso é possível; aumento na complexidade do sistema; Dependencia de um (ou mais) fornecedor(es), *vendor lock-in*.

### Containers

Outra opção interessante é a utilização de *containers*, eles nos possibilitam a abstração quase que completa do sistema em que são executados, seja ele em diversos sistemas operacionais, VMs ou até mesmo em núvem.

Algumas vantagens desse modelo são garantia que alterações realizadas internamente ao container seja removida na próxima construção do container, trazendo um ambiente "limpo" e reproduzível; são de fácil configuração; não é necessário compartilhar uma imagem do sistema inteiro (como seria com uma VM), apenas o arquivo de configuração; fácil automação de processos.

Esse modelo funciona bem com a arquitetura de micro serviços, garantindo que cada micro serviço não interfira em outro. A vantagem de garantir que os dados do container sejam sempre destruídos pode se tornar um problema (ou complicador) quando é necessário salvar algum dado, aumentando ainda mais a complexidade do sistema de modo geral. 

### VMs

O modelo de VMs é o mais símples e compatível com aplicações já existentes do que os modelos citados anteriormente. O objetivo de usar VMs é para a segmentar os diversos serviços trazendo mais segurança e, geralmente, desempenho já que os recursos são limitados e pré definidos para cada VM.

A principal vantagem desse modelo é a compatibilidade com sistemas já existentes e uma vasta documentação com exemplos detalhados na internet.

Esse modelo traz algumas desvantagens, a principal sendo o *overhead* dos recursos pois é necessário alocar uma quantia fixa para cada VM e, geralmente, esses recursos ficam ociosos pois a(s) aplicação(ões) em execução dentro da VM não utilizam 100% dos recursos disponíveis. Outra desvantagem é o aumento na complexidade de manutenção do parque computacional já que cada VM possui seu próprio sistema operacional com seus pacotes.

### Proposta

Minha proposta, baseado na conversa com o Stevão Porfirio, é começar com um misto do sistema de Containers e VMs já que, a princípio, não existe um sistema construído e o prazo parece ser pequeno, futuramente analisar a viabilidade de migração para o modelo *serverless*.

Proponho a seguinte arquitetura: Múltiplas VMs para produção e homologação, cada uma com os micro serviçoes dentro de containers. A gestão dos containers pode ser feita por meio da ferramenta [Docker Swarm] ou [Kubernetes].
![Arquitetura de VMs e Containers][architecture]

## Infraestrutura Local

A infraestrutura local é muito similar à remota proposta anteriormente, a diferença é que não é necessário o uso de VMs, basta instalar as mesmas ferramentas (exemplo: Docker Swarm ou Kubernetes e Docker). 

## Desenvolvimento

O desenvolvimento das aplicações deverá ser feito utilizando um sistema de controle de versão
(exemplo: Git) em conjunto com um sistema de integração contínua com testes (CI). Todas as funcionalidades deverão ter testes específicos de corretude e desempenho com intuito de obter uma cobertura de 100% do código.

## Deploy e Manutenção

Os deploys e manutenções deverão ser realizados através de ferramentas automatização (exemplo: Ansible) para que haja uma redução da possibilidade de erro humano no processo.





   [Have I Been Pwned]: <https://www.troyhunt.com/serverless-to-the-max-doing-big-things-for-small-dollars-with-cloudflare-workers-and-azure-functions/>
   [Troy Hunt]: <https://www.troyhunt.com/about/>
   [Microsoft Azure Functions]: <https://azure.microsoft.com/en-us/services/functions/>
   [Cloudflare Workers]: <https://www.cloudflare.com/en-us/products/cloudflare-workers/>
   [architecture]: <https://gitlab.com/andreziviani/projeto-barigui/raw/master/figs/arquitetura.png>
   [Docker Swarm]: <https://docs.docker.com/engine/swarm/>
   [Kubernetes]: <https://kubernetes.io/>
 
